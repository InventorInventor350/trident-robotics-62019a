void driveIntake(int speed) {

	motor[intake] = speed;

}

void intakeDriverCode(int speed, int flipFlop) {

	if(vexRT[Btn6UXmtr2]) {

		driveIntake(speed * flipFlop);

	}

	else if(vexRT[Btn6DXmtr2]) {

		driveIntake(speed * flipFlop * -1);

	}

	else {

		driveIntake(0);

	}

}
