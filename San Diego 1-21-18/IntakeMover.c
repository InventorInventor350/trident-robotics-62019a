void driveIntakeMover(int speed) {

	motor[intakeMover] = speed;

}

void intakeMoverDriverCode(int speed, int flipFlop) {

	if(vexRT[Btn5UXmtr2]) {

		driveIntakeMover(speed * flipFlop);

	}

	else if(vexRT[Btn5DXmtr2]) {

		driveIntakeMover(speed * flipFlop * -1);

	}

	else {

		driveIntakeMover(0);

	}

}
