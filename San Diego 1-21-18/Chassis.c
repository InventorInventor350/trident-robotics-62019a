void driveLeftChassisSide(int speed) {

	motor[leftBackChassis] = speed;
	motor[leftFrontChassis] = speed;

}

void driveRightChassisSide(int speed) {

	motor[rightBackChassis] = speed;
	motor[rightFrontChassis] = speed;

}

void resetChassisEncoderValues() {

	SensorValue[leftBackChassisEncoder] = 0;
	SensorValue[rightBackChassisEncoder] = 0;

}

void chassisDriverCode(int minJoystickValue) {

	if(fabs(vexRT[Ch3]) >= minJoystickValue) {

		driveLeftChassisSide(vexRT[Ch3]);

	}

	else {

		driveLeftChassisSide(0);

	}

	if(fabs(vexRT[Ch2]) >= minJoystickValue) {

		driveRightChassisSide(vexRT[Ch2]);

	}

	else {

		driveRightChassisSide(0);

	}

}

void driveChassisWithTimeControl(int leftSpeed, int leftDirection, int rightSpeed, int rightDirection, int sec) {

	driveLeftChassisSide(leftSpeed * leftDirection);
	driveRightChassisSide(rightSpeed * rightDirection);
	wait1Msec(sec * 1000);

}

void driveChassisWithoutSpecialControl(int leftEncoderValue, int leftSpeed, int leftDirection, int rightEncoderValue, int rightSpeed, int rightDirection) {

	resetChassisEncoderValues();
	driveLeftChassisSide(0);
	driveRightChassisSide(0);

	while(leftChassisEncoder < leftEncoderValue || rightChassisEncocder < rightEncoderValue) {

		if(leftChassisEncoder < leftEncoderValue) {

			driveLeftChassisSide(leftSpeed * leftDirection);

		}

		else {

			driveLeftChassisSide(0);

		}

		if(rightChassisEncocder < rightEncoderValue) {

			driveRightChassisSide(rightSpeed * rightDirection);

		}

		else {

			driveRightChassisSide(0);

		}

	}

	driveLeftChassisSide(0);
	driveRightChassisSide(0);
	resetChassisEncoderValues();

}

void driveChassisWithSimplePSM(int leftEncoderValue, int leftSpeed, int leftDirection, int rightEncoderValue, int rightSpeed, int rightDirection) {

	resetChassisEncoderValues();
	driveLeftChassisSide(0);
	driveRightChassisSide(0);

	while(leftChassisEncoder < leftEncoderValue * (0.6) || rightChassisEncocder < rightEncoderValue * (0.6)) {

		if(leftChassisEncoder < leftEncoderValue * (0.6)) {

			driveLeftChassisSide(fabs(leftSpeed) * leftDirection);

		}

		else {

			driveLeftChassisSide(0);

		}

		if(rightChassisEncocder < rightEncoderValue * (0.6)) {

			driveRightChassisSide(fabs(rightSpeed) * rightDirection);

		}

		else {

			driveRightChassisSide(0);

		}

	}

	while(leftChassisEncoder < leftEncoderValue || rightChassisEncocder < rightEncoderValue) {

		if(leftChassisEncoder < leftEncoderValue) {

			driveLeftChassisSide(fabs(leftSpeed / 3) * leftDirection);

		}

		else {

			driveLeftChassisSide(0);

		}

		if(rightChassisEncocder < rightEncoderValue) {

			driveRightChassisSide(fabs(rightSpeed / 3) * rightDirection);

		}

		else {

			driveRightChassisSide(0);

		}

	}

	driveChassisWithTimeControl(((fabs(leftSpeed) * leftDirection)/2), -1, ((fabs(rightSpeed) * rightDirection)/2), -1, 0.05);

	driveLeftChassisSide(0);
	driveRightChassisSide(0);
	resetChassisEncoderValues();

}

void driveStraightWithMasterSlaveSystem(int encoderValueSetPoint, int kP, int masterPower, int direction, int slavePowerSetBack) {

	int totalTicks = 0;
	int slavePower = masterPower - slavePowerSetBack;
	int error = 0;

	resetChassisEncoderValues();
	driveLeftChassisSide(0);
	driveRightChassisSide(0);

	while(fabs(totalTicks) < encoderValueSetPoint) {

		driveLeftChassisSide(masterPower);
		driveRightChassisSide(slavePower);

		error = leftChassisEncoder - rightChassisEncocder;
		slavePower += error / kP;

		wait1Msec(100);

		totalTicks += SensorValue[leftBackChassisEncoder];

	}

	driveLeftChassisSide(0);
	driveRightChassisSide(0);
	resetChassisEncoderValues();

}
