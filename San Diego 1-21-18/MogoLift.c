void driveMogoLift(int speed) {

	motor[mogoLift] = speed;

}

void mogoLiftDriverCodeWithoutSensors(int speed, int flipFlop) {

	if(vexRT[Btn5U]) {

		driveMogoLift(speed * flipFlop);

	}

	else if(vexRT[Btn5D]) {

		driveMogoLift(-speed * flipFlop);

	}

	else {

		driveMogoLift(0);

	}

}

void mogoLiftDriverWithSensors(int speed, int flipFlop) {

	bool otherMogoFunctionIsRunning = false;

	if(vexRT[Btn5U] && !otherMogoFunctionIsRunning) {

		otherMogoFunctionIsRunning = true;

		while(!SensorValue[mogoIsOutLeft] || !SensorValue[mogoIsOutRight]) {

			driveMogoLift(speed * flipFlop);

			chassisDriverCode(MinJoystickValueForChassis);

			mainLiftDriverControl(mainLiftFlipFlop, MinJoystickValueForMainLift);

			intakeMoverDriverCode(intakeMoverSpeed, intakeMoverFlipFlop);

			intakeDriverCode(intakeSpeed, intakeFlipFlop);

			wait1Msec(20);

			otherMogoFunctionIsRunning = true;

		}

		driveMogoLift(0);
		otherMogoFunctionIsRunning = false;

	}

	if(vexRT[Btn5D] && !otherMogoFunctionIsRunning) {

		while(!SensorValue[mogoIsInLeft] || !SensorValue[mogoIsInRight]) {

			driveMogoLift(speed * flipFlop * -1);

			chassisDriverCode(MinJoystickValueForChassis);

			mainLiftDriverControl(mainLiftFlipFlop, MinJoystickValueForMainLift);

			intakeDriverCode(intakeSpeed, intakeFlipFlop);

			intakeMoverDriverCode(intakeMoverSpeed, intakeMoverFlipFlop);

			wait1Msec(20);

			otherMogoFunctionIsRunning = true;

		}

		driveMogoLift(0);
		otherMogoFunctionIsRunning = false;

	}

}
