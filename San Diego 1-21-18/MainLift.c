void driveMainLift(int speed) {

	motor[liftMotors] = speed;

}

void mainLiftDriverControl(int flipFlop, int minJoystickValue) {

	if(fabs(vexRT[Ch3Xmtr2]) >= minJoystickValue) {

		driveMainLift(vexRT[Ch3Xmtr2] * flipFlop);

	}

	else {

		driveMainLift(0);

	}

}
