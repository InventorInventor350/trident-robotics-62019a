#pragma platform(VEX2)									//DO NOT EDIT; Robot Platform
#pragma competitionControl(Competition) //DO NOT EDIT; Competition Control
#include "Vex_Competition_Includes.c"   //DO NOT EDIT; Include Official Competition Control File

void oneControllerScheme() {

	driveLeftChassisSide(((fabs(vexRT[Ch3]) >= arcadeControlDeadBand) ? (vexRT[Ch3]) : (0)) + ((fabs(vexRT[Ch4]) >= arcadeControlDeadBand) ? (vexRT[Ch4]) : (0)));
	driveRightChassisSide(((fabs(vexRT[Ch3]) >= arcadeControlDeadBand) ? (vexRT[Ch3]) : (0)) - ((fabs(vexRT[Ch4]) >= arcadeControlDeadBand) ? (vexRT[Ch4]) : (0)));

	if(vexRT[Btn7U]) {

		driveMogoLift(120);

	}

	else if(vexRT[Btn8U]) {

		driveMogoLift(-120);

	}

	else {

		driveMogoLift(0);

	}

	/*bool otherMogoFunctionIsRunning = false;

	if(vexRT[Btn5U] && !otherMogoFunctionIsRunning) {

		otherMogoFunctionIsRunning = true;

		while(!SensorValue[mogoIsOutLeft] || !SensorValue[mogoIsOutRight]) {

			driveMogoLift(MogoSpeed * MogoFlipFlop);

			driveLeftChassisSide(((fabs(vexRT[Ch3]) >= arcadeControlDeadBand) ? (vexRT[Ch3]) : (0)) + ((fabs(vexRT[Ch4]) >= arcadeControlDeadBand) ? (vexRT[Ch4]) : (0)));
			driveRightChassisSide(((fabs(vexRT[Ch3]) >= arcadeControlDeadBand) ? (vexRT[Ch3]) : (0)) - ((fabs(vexRT[Ch4]) >= arcadeControlDeadBand) ? (vexRT[Ch4]) : (0)));

			driveMainLift(vexRT[Ch2] * mainLiftFlipFlop);

			if(vexRT[Btn6U]) {

				driveIntakeMover(intakeMoverSpeed * intakeFlipFlop);

			}

			else if(vexRT[Btn6D]) {

				driveIntakeMover(intakeMoverSpeed * intakeFlipFlop * -1);

			}

			else {

				driveIntakeMover(0);

			}

			if(vexRT[Btn7U]) {

				driveIntake(intakeSpeed * intakeFlipFlop);

			}

			else if(vexRT[Btn8U]) {

				driveIntake(intakeSpeed * intakeFlipFlop * -1);

			}

			else {

				driveIntake(0);

			}

			wait1Msec(20);

			otherMogoFunctionIsRunning = true;

		}

		driveMogoLift(0);
		otherMogoFunctionIsRunning = false;

	}

	if(vexRT[Btn5D] && !otherMogoFunctionIsRunning) {

		while(!SensorValue[mogoIsInLeft] || !SensorValue[mogoIsInRight]) {

			driveMogoLift(MogoSpeed * MogoFlipFlop * -1);

			driveLeftChassisSide(((fabs(vexRT[Ch3]) >= arcadeControlDeadBand) ? (vexRT[Ch3]) : (0)) + ((fabs(vexRT[Ch4]) >= arcadeControlDeadBand) ? (vexRT[Ch4]) : (0)));
			driveRightChassisSide(((fabs(vexRT[Ch3]) >= arcadeControlDeadBand) ? (vexRT[Ch3]) : (0)) - ((fabs(vexRT[Ch4]) >= arcadeControlDeadBand) ? (vexRT[Ch4]) : (0)));

			driveMainLift(vexRT[Ch2] * mainLiftFlipFlop);

			if(vexRT[Btn6U]) {

				driveIntakeMover(intakeMoverSpeed * intakeFlipFlop);

			}

			else if(vexRT[Btn6D]) {

				driveIntakeMover(intakeMoverSpeed * intakeFlipFlop * -1);

			}

			else {

				driveIntakeMover(0);

			}

			if(vexRT[Btn7U]) {

				driveIntake(intakeSpeed * intakeFlipFlop);

			}

			else if(vexRT[Btn8U]) {

				driveIntake(intakeSpeed * intakeFlipFlop * -1);

			}

			else {

				driveIntake(0);

			}

			wait1Msec(20);

			otherMogoFunctionIsRunning = true;

		}

		driveMogoLift(0);
		otherMogoFunctionIsRunning = false;

	}*/

	driveMainLift(vexRT[Ch2] * mainLiftFlipFlop);

	if(vexRT[Btn6U]) {

		driveIntakeMover(intakeMoverSpeed * intakeFlipFlop);

	}

	else if(vexRT[Btn6D]) {

		driveIntakeMover(intakeMoverSpeed * intakeFlipFlop * -1);

	}

	else {

		driveIntakeMover(0);

	}

	if(vexRT[Btn5U]) {

		driveIntake(intakeSpeed * intakeFlipFlop);

	}

	else if(vexRT[Btn5D]) {

		driveIntake(intakeSpeed * intakeFlipFlop * -1);

	}

	else {

		driveIntake(0);

	}

}

void pre_auton() { //Pre_Auton

	bStopTasksBetweenModes = true;

}

task autonomous() { //Autonomous

	//AutonomousCodePlaceholderForTesting();

	//driveChassisWithSimplePSM(500, 120, chassisForward, 400, 120, chassisForward);

	wait1Msec(5000);

	driveStraightWithMasterSlaveSystem(50000, chassisKP, 80, chassisForward, chassisSlavePowerSetBack);

}

task usercontrol() { //Driver

	while(true) { //Infinite Loop

		//UserControlCodePlaceholderForTesting();

		chassisDriverCode(MinJoystickValueForChassis);

		mogoLiftDriverCodeWithoutSensors(MogoSpeed, MogoFlipFlop);

		//mogoLiftDriverWithSensors(MogoSpeed, MogoFlipFlop);

		mainLiftDriverControl(mainLiftFlipFlop, MinJoystickValueForMainLift);

		intakeMoverDriverCode(intakeMoverSpeed, intakeMoverFlipFlop);

		intakeDriverCode(intakeSpeed, intakeFlipFlop);

		//oneControllerScheme();

		wait1Msec(20);

	}

}
