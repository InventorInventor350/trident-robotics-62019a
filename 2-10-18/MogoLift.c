const int mogoLiftSpeed = 125;
const int mogoLiftFlipFlop = 1;

void driveMogoLift(int speed) {

	motor[mogoLift] = speed;

}

void mogoLiftDriverControl(int speed, int flipFlop) {

	if(vexRT[Btn5D] && (!(SensorValue[leftMogoLiftIsIn]) || !(SensorValue[rightMogoLiftIsIn]))) {

		driveMogoLift(speed * flipFlop);

	}

	else if(vexRT[Btn5U] && (!(SensorValue[leftMogoLiftIsOut]) || !(SensorValue[rightMogoLiftIsOut]))) {

		driveMogoLift(speed * flipFlop * -1);

	}

	else {

		driveMogoLift(0);

	}

}

void putMogoLiftOut() {

	while(!(SensorValue[leftMogoLiftIsOut]) && !(SensorValue[rightMogoLiftIsOut])) {

		driveMogoLift(-120);
		wait1Msec(20);

	}

	driveMogoLift(0);

}

void putMogoLiftIn() {

	while(!(SensorValue[leftMogoLiftIsIn]) && !(SensorValue[rightMogoLiftIsIn])) {

		driveMogoLift(120);
		wait1Msec(20);

	}

	driveMogoLift(0);

}
