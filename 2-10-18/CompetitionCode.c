#pragma platform(VEX2)									//DO NOT EDIT
#pragma competitionControl(Competition) //DO NOT EDIT
#include "Vex_Competition_Includes.c"   //DO NOT EDIT

void pre_auton() { //Pre Auton Stage

	bStopTasksBetweenModes = true;

	SensorType[chassisGyro] = sensorNone;
	wait1Msec(100);
	SensorType[chassisGyro] = sensorGyro;
	wait1Msec(2000);
	SensorValue[leftBackChassisEncoder] = 0;
	SensorValue[rightBackChassisEncoder] = 0;
	SensorValue[mainLiftEncoder] = 0;
	wait1Msec(500);

}

task autonomous() { //Autonomous Stage

	//AutonomousCodePlaceholderForTesting();
	blueLeftMogoToTenPoint();

}

task usercontrol() { //Driver Control Stage

	while(true) { //Infinite loop to allow drive control stage to be continuous

		//UserControlCodePlaceholderForTesting();

		chassisDriverControl(minJoystickValueForChassis);

		mainLiftDriverControl(minJoystickValueForMainLift, mainLiftFlipFlop);

		mogoLiftDriverControl(mogoLiftSpeed, mogoLiftFlipFlop);

		intakeMoverDriverControl(intakeMoverSpeed, intakeMoverFlipFlop);

		intakeDriverControl(intakeSpeed, intakeFlipFlop);

		/*driveLeftChassis(((fabs(vexRT[Ch3]) >= 30) ? (vexRT[Ch3]) : (0)) + ((fabs(vexRT[Ch4]) >= 30) ? (vexRT[Ch4]) : (0)));
		driveRightChassis(((fabs(vexRT[Ch3]) >= 30) ? (vexRT[Ch3]) : (0)) - ((fabs(vexRT[Ch4]) >= 30) ? (vexRT[Ch4]) : (0)));

		driveMainLift(fabs(vexRT[Ch2]) > 30 ? (vexRT[Ch2] < 0 ? (vexRT[Ch2] / 2) : (vexRT[Ch2])) : 0);

		if(vexRT[Btn7U] && (!(SensorValue[leftMogoLiftIsIn]) || !(SensorValue[rightMogoLiftIsIn]))) {

			driveMogoLift(120 * 1);

		}

		else if(vexRT[Btn8U] && (!(SensorValue[leftMogoLiftIsOut]) || !(SensorValue[rightMogoLiftIsOut]))) {

			driveMogoLift(120 * 1 * -1);

		}

		else {

			driveMogoLift(0);

		}

		if(vexRT[Btn6U]) {

			driveIntakeMover(120 * -1);

		}

		else if(vexRT[Btn6D]) {

			driveIntakeMover(120 * -1 * -1);

		}

		else {

			driveIntakeMover(0);

		}


		if(vexRT[Btn5U]) {

			driveIntake(120 * -1);

		}

		else if(vexRT[Btn5D]) {

			driveIntake(120 * -1 * -1);

		}

		else {

			driveIntake(0);
*/

		wait1Msec(20);

	}

}
