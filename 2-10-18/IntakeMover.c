const int intakeMoverSpeed = 125;
const int intakeMoverFlipFlop = -1;

void driveIntakeMover(int speed) {

	motor[intakeMover] = speed;

}

void intakeMoverDriverControl(int speed, int flipFlop) {

	if(vexRT[Btn6UXmtr2]) {

		driveIntakeMover(speed * flipFlop);

	}

	else if(vexRT[Btn6DXmtr2]) {

		driveIntakeMover(speed * flipFlop * -1);

	}

	else {

		driveIntakeMover(0);

	}

}

void driveIntakeMoverForSpecificTime(int speed, int mSec) {

	driveIntakeMover(speed);
	wait1Msec(mSec);
	driveIntakeMover(0);

}
