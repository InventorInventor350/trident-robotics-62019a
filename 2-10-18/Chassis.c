const int minJoystickValueForChassis = 30;
const int goingStraightKPWithGyro = 10;
const int goingStraightKPWithEncoders = 9;
const int leftSpeedSetBack = 0;
const int errorThreshold = 0;
const int turningWellKP = 1;
const int masterSlaveKP = 60;
const int slavePowerSetBack = 5;

void driveLeftChassis(int speed) {

	motor[leftBackChassis] = speed;
	motor[leftFrontChassis] = speed;

}

void driveRightChassis(int speed) {

	motor[rightBackChassis] = speed;
	motor[rightFrontChassis] = speed;

}

void chassisDriverControl(int minJoystickValue) {

	driveLeftChassis(fabs(vexRT[Ch3]) > minJoystickValue ? vexRT[Ch3] : 0);
	driveRightChassis(fabs(vexRT[Ch2]) > minJoystickValue ? vexRT[Ch2] : 0);

}

void driveChassisStraightWithGyro(int setPoint, int speed, int direction) {

	driveLeftChassis(0);
	driveRightChassis(0);
	SensorValue[leftBackChassisEncoder] = 0;
	SensorValue[rightBackChassisEncoder] = 0;
	wait1Msec(20);

	int gyroSetPoint = chassisGyroSensorValue;
	int leftChassisSpeed = speed;
	int rightChassisSpeed = speed;

	while(leftChassisEncoder < setPoint || rightChassisEncoder < setPoint) {

		if(leftChassisEncoder < setPoint) {

			driveLeftChassis(leftChassisSpeed * direction);
			leftChassisSpeed += (((chassisGyroSensorValue - gyroSetPoint) / 2) / goingStraightKPWithGyro);

		}

		else {

			driveLeftChassis(0);

		}

		if(rightChassisEncoder < setPoint) {

			driveRightChassis(rightChassisSpeed * direction);
			rightChassisSpeed += -(((chassisGyroSensorValue - gyroSetPoint) / 2) / goingStraightKPWithGyro);

		}

		else {

			driveRightChassis(0);

		}

		wait1Msec(50);

	}

	driveLeftChassis(0);
	driveRightChassis(0);
	SensorValue[leftBackChassisEncoder] = 0;
	SensorValue[rightBackChassisEncoder] = 0;
	wait1Msec(20);

}

void driveChassisStraightWithEncoders(int setPoint, int speed, int direction) {

	driveLeftChassis(0);
	driveRightChassis(0);
	SensorValue[leftBackChassisEncoder] = 0;
	SensorValue[rightBackChassisEncoder] = 0;
	wait1Msec(20);

	int leftChassisSpeed = speed;
	int rightChassisSpeed = speed;

	while(leftChassisEncoder < setPoint || rightChassisEncoder < setPoint) {

		if(leftChassisEncoder < setPoint) {

			driveLeftChassis(leftChassisSpeed * direction);
			leftChassisSpeed += -((((leftChassisEncoder - rightChassisEncoder) > errorThreshold ? (leftChassisEncoder - rightChassisEncoder) : 0) / 2) / goingStraightKPWithEncoders);

		}

		else {

			driveLeftChassis(0);

		}

		if(rightChassisEncoder < setPoint) {

			driveRightChassis(rightChassisSpeed * direction);
			rightChassisSpeed += ((((leftChassisEncoder - rightChassisEncoder) > errorThreshold ? (leftChassisEncoder - rightChassisEncoder) : 0) / 2) / goingStraightKPWithEncoders);

		}

		else {

			driveRightChassis(0);

		}

		wait1Msec(50);

	}

	driveLeftChassis(0);
	driveRightChassis(0);
	SensorValue[leftBackChassisEncoder] = 0;
	SensorValue[rightBackChassisEncoder] = 0;
	wait1Msec(20);

}

void driveChassisSidesIndependently(int leftSetPoint, int leftSpeed, int leftDirection, int rightSetPoint, int rightSpeed, int rightDirection) {

	while(leftChassisEncoder < leftSetPoint || rightChassisEncoder < rightSetPoint) {

		if(leftChassisEncoder < leftSetPoint) {

			driveLeftChassis(fabs(leftSpeed) * leftDirection);

		}

		else {

			driveLeftChassis(0);
			SensorValue[leftBackChassisEncoder] = 0;

		}

		if(rightChassisEncoder < rightSetPoint) {

			driveRightChassis(fabs(rightSpeed) * rightDirection);

		}

		else {

			driveRightChassis(0);
			SensorValue[rightBackChassisEncoder] = 0;

		}

	}

}

void driveChassisStraightUsingMasterSlave(int masterPower, int direction, int setPoint) {

	int slavePower = masterPower - slavePowerSetBack;
	int error = 0;
	int totalTicks = 0;

	while(abs(totalTicks) < setPoint) {

		driveRightChassis(slavePower * direction);
		driveLeftChassis(masterPower * direction);

		error = SensorValue[leftBackChassisEncoder] - SensorValue[rightBackChassisEncoder];

		slavePower += error / masterSlaveKP;

		SensorValue[leftBackChassisEncoder] = 0;
		SensorValue[rightBackChassisEncoder] = 0;

		wait1Msec(100);

		totalTicks += SensorValue[leftBackChassisEncoder];

	}

	driveLeftChassis(0);
	driveRightChassis(0);
	SensorValue[leftBackChassisEncoder] = 0;
	SensorValue[rightBackChassisEncoder] = 0;

}

void pivotChassis(int angleChange, int speed, int direction) {

	driveLeftChassis(0);
	driveRightChassis(0);
	SensorValue[leftBackChassisEncoder] = 0;
	SensorValue[rightBackChassisEncoder] = 0;
	wait1Msec(20);

	int gyroSetPoint = fabs(chassisGyroSensorValue) + angleChange;
	int leftChassisSpeed = speed;
	int rightChassisSpeed = speed;

	while(fabs(chassisGyroSensorValue) < gyroSetPoint) {

		driveLeftChassis(leftChassisSpeed * direction);
		driveRightChassis(rightChassisSpeed * direction * -1);
		wait1Msec(50);
		leftChassisSpeed += -((((leftChassisEncoder - rightChassisEncoder) > errorThreshold ? (leftChassisEncoder - rightChassisEncoder) : 0) / 2) / turningWellKP);
		rightChassisSpeed += ((((leftChassisEncoder - rightChassisEncoder) > errorThreshold ? (leftChassisEncoder - rightChassisEncoder) : 0) / 2) / turningWellKP);


	}

	driveLeftChassis(0);
	driveRightChassis(0);
	SensorValue[leftBackChassisEncoder] = 0;
	SensorValue[rightBackChassisEncoder] = 0;
	wait1Msec(20);

}
