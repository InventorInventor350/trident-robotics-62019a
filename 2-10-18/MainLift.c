const int minJoystickValueForMainLift = 30;
const int mainLiftFlipFlop = 1;

void driveMainLift(int speed) {

	motor[leftMainLift] = speed;
	motor[rightMainLift] = speed;

}

void mainLiftDriverControl(int minJoystickValue, int flipFlop) {

	driveMainLift(fabs(vexRT[Ch2Xmtr2]) > 30 ? (vexRT[Ch2Xmtr2] < 0 ? (vexRT[Ch2Xmtr2]) : (vexRT[Ch2Xmtr2])) : 0);

}

void moveMainLiftToSpecificEncoderValue(int setPoint, int speed) {

	if(mainLiftEncoderValue < setPoint) {

		while(mainLiftEncoderValue < setPoint) {

			driveMainLift(speed);
			wait1Msec(20);

		}

		driveMainLift(0);

	}

	else if(mainLiftEncoderValue > setPoint) {

		while(mainLiftEncoderValue > setPoint) {

			driveMainLift(-speed);
			wait1Msec(20);

		}

		driveMainLift(0);

	}

	driveMainLift(0);

}
