const int intakeSpeed = 125;
const int intakeFlipFlop = -1;

void driveIntake(int speed) {

	motor[intake] = speed;

}

void intakeDriverControl(int speed, int flipFlop) {

	if(vexRT[Btn5UXmtr2]) {

		driveIntake(speed * flipFlop);

	}

	else if(vexRT[Btn5DXmtr2]) {

		driveIntake(speed * flipFlop * -1);

	}

	else {

		driveIntake(0);

	}

}

void driveIntakeForSpecificTime(int speed, int mSec) {

	driveIntake(speed);
	wait1Msec(mSec);
	driveIntake(0);

}
