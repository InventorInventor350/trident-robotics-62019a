#pragma config(UART_Usage, UART1, uartVEXLCD, baudRate19200, IOPins, None, None)
#pragma config(UART_Usage, UART2, uartNotUsed, baudRate4800, IOPins, None, None)
#pragma config(I2C_Usage, I2C1, i2cSensors)
#pragma config(Sensor, in1,    chassisGyro,    sensorGyro)
#pragma config(Sensor, in2,    intakeMoverPotent, sensorPotentiometer)
#pragma config(Sensor, dgtl1,  leftMogoLiftIsIn, sensorTouch)
#pragma config(Sensor, dgtl2,  rightMogoLiftIsIn, sensorTouch)
#pragma config(Sensor, dgtl3,  leftMogoLiftIsOut, sensorTouch)
#pragma config(Sensor, dgtl4,  rightMogoLiftIsOut, sensorTouch)
#pragma config(Sensor, dgtl6,  mainLiftEncoder, sensorQuadEncoder)
#pragma config(Sensor, I2C_1,  leftBackChassisEncoder, sensorQuadEncoderOnI2CPort,    , AutoAssign )
#pragma config(Sensor, I2C_2,  rightBackChassisEncoder, sensorQuadEncoderOnI2CPort,    , AutoAssign )
#pragma config(Motor,  port2,           leftBackChassis, tmotorVex393_MC29, openLoop, encoderPort, I2C_1)
#pragma config(Motor,  port3,           rightBackChassis, tmotorVex393_MC29, openLoop, reversed, encoderPort, I2C_2)
#pragma config(Motor,  port4,           leftFrontChassis, tmotorVex393_MC29, openLoop)
#pragma config(Motor,  port5,           rightFrontChassis, tmotorVex393_MC29, openLoop, reversed)
#pragma config(Motor,  port6,           mogoLift,      tmotorVex393_MC29, openLoop)
#pragma config(Motor,  port7,           leftMainLift,  tmotorVex393HighSpeed_MC29, openLoop)
#pragma config(Motor,  port8,           rightMainLift, tmotorVex393HighSpeed_MC29, openLoop)
#pragma config(Motor,  port9,           intakeMover,   tmotorVex393HighSpeed_MC29, openLoop)
#pragma config(Motor,  port10,          intake,        tmotorVex393_HBridge, openLoop)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//

#define chassisGyroSensorValue (SensorValue[chassisGyro] * 0.0939735)
#define leftChassisEncoder (fabs(SensorValue[leftBackChassisEncoder]))
#define rightChassisEncoder (fabs(SensorValue[rightBackChassisEncoder]))
#define mainLiftEncoderValue (fabs(SensorValue[mainLiftEncoder]))

#include "Chassis.c"
#include "NewChassis.c"
#include "NewNewChassis.c"
#include "MogoLift.c"
#include "MainLift.c"
#include "IntakeMover.c"
#include "Intake.c"
#include "Autons.c"
#include "CompetitionCode.c"
