const int distP = 1.0;
const int distI = 0.005;
const int distD = 1.0;
const int diffP = 1.0;
const int diffI = 0.005;
const int diffD = 1.0;

void resetEncoders() {

	SensorValue[leftBackChassisEncoder] = 0;
	SensorValue[rightBackChassisEncoder] = 0;

}

void driveStraight(int distance, int maxspeed, int time) {

	resetEncoders();

	int prevdisterror = 0;
	int prevdifferror = 0;

	int disterror, differror, distintegral, diffintegral, distderivative, diffderivative, distspeed, diffspeed;

	clearTimer(T1);

	while (time1[T1] < time) {

		disterror = distance - ((SensorValue[leftBackChassisEncoder] + SensorValue[rightBackChassisEncoder])/2); //Calculate distance error
		differror = SensorValue[leftBackChassisEncoder] - SensorValue[rightBackChassisEncoder]; //Calculate difference error

		// Find the integral ONLY if within controllable range AND if the distance error is not equal to zero
		if( abs(disterror) < 60 && disterror != 0) {

			distintegral = distintegral + disterror;

		}

		else {

			distintegral = 0; //Otherwise, reset the integral

		}

		// Find the integral ONLY if within controllable range AND if the difference error is not equal to zero
		if( abs(differror) < 60 && differror != 0) {

			diffintegral = diffintegral + differror;

		}

		else{

			diffintegral = 0; //Otherwise, reset the integral

		}

		distderivative = disterror - prevdisterror; //Calculate distance derivative
		diffderivative = differror - prevdifferror; //Calculate difference derivative

		prevdisterror = disterror; //Update previous distance error
		prevdifferror = differror; //Update previous difference error

		distspeed = (disterror * distP) + (distintegral * distI) + (distderivative * distD); //Calculate distance speed
		diffspeed = (differror * diffP) + (diffintegral * diffI) + (diffderivative* diffD); //Calculate difference (turn) speed

		if(distspeed > maxspeed) { //Check that the speed is not exceeding the maximum set speed

			distspeed = maxspeed;

		}

		if(distspeed < -maxspeed) { //Check that the speed is not exceeding the maximum set speed

			distspeed = -maxspeed;

		}

		driveLeftChassis(distspeed - diffspeed); //Set motor values
		driveRightChassis(distspeed + diffspeed); //Set motor values

		wait1Msec(20); //Give the robot a (quick) break
	}
}
