void blueLeftPreloadToStationary() {

	driveIntakeMoverForSpecificTime(120, 1000);
	moveMainLiftToSpecificEncoderValue(30, 120);
	nnDriveChassisStraight(825, 80, 1);
	wait1Msec(1000);
	moveMainLiftToSpecificEncoderValue(25, 60);
	motor[intake] = 120;
	driveMainLift(60);
	wait1Msec(250);
	motor[intake] = 0;
	driveMainLift(0);
	wait1Msec(1000);
	nnDriveChassisStraight(300, 80, -1);

}

void blueRightPreloadToStationary() {



}

void redLeftPreloadToStationary() {



}

void redRightPreloadToStationary() {



}

void blueLeftMogoToTenPoint() {

	driveIntakeMoverForSpecificTime(120, 1000);
	putMogoLiftOut();
	nnDriveChassisStraight(2250, 80, 1);
	putMogoLiftIn();
	driveIntakeMoverForSpecificTime(120, 500);
	driveIntakeForSpecificTime(120, 100);
	nnDriveChassisStraight(1750, 80, -1);
	nnPivotChassisWell(1100, 80, -1);

}

void blueRightMogoToTenPoint() {



}

void redLeftMogoToTenPoint() {



}

void redRightMogoToTenPoint() {



}
