const int nnKP = 10; //kP constant for nnDriveChassisStraight
const int nnKI = 1; //kI constant for nnDriveChassisStraight
const int nnKD = 1; //kD constant for nnDriveChassisStraight
const bool useNNKP = true; //for getting rid of parts of PID, since
const bool useNNKI = false; //true is equal to 1 and false is equal to 0,
const bool useNNKD = false; //then you can get rid of parts by multiplying by 0 since it'll equal 0
const int controllableRange = 50; //used for deciding if the error is in a controllable range for integral

void nnDriveChassisStraight(int setPoint, int speed, int direction) {

	driveLeftChassis(0);
	driveRightChassis(0);
	resetEncoders();

	int leftChassisSpeed = speed;
	int rightChassisSpeed = speed;

	int error, integral, derivative, prevError;

	while(leftChassisEncoder < setPoint && rightChassisEncoder < setPoint) {

		error = (leftChassisEncoder - rightChassisEncoder) / 2; //find average error that can be applied to both sides
		integral += ((fabs(error) < controllableRange) && (error != 0)) ? (error) : (0); //if error is in a controllable range and is not equal to zero, then increment it by the current error
		derivative = error - prevError; //derivative is equal to the error minus the previous error
		prevError = error; //set previous error to current error so it can be used on the next iteration
		leftChassisSpeed -= ((error / nnKP) * useNNKP) + ((integral / nnKP) * useNNKI) + ((derivative / nnKD) * useNNKD); //decrement left speed by error times constant plus integral times constant plus derivative times constant
		rightChassisSpeed += ((error / nnKP) * useNNKP) + ((integral / nnKP) * useNNKI) + ((derivative / nnKD) * useNNKD); //increment right speed by error times constant plus integral times constant plus derivative times constant
		driveLeftChassis(leftChassisSpeed * direction); //apply newly calculated speed to left chassis
		driveRightChassis(rightChassisSpeed * direction); //apply newly calculated speed to right chassis
		wait1Msec(20); //iterate 1000 divided 20 times a second (or 50 times a second)

	}

	driveLeftChassis(0);
	driveRightChassis(0);
	resetEncoders();

}

void nnPivotChassisWell(int setPoint, int speed, int direction) {

	driveLeftChassis(0);
	driveRightChassis(0);
	resetEncoders();

	int leftChassisSpeed = speed;
	int rightChassisSpeed = speed;

	int error, integral, derivative, prevError;

	while(leftChassisEncoder < setPoint && rightChassisEncoder < setPoint) {

		error = (leftChassisEncoder - rightChassisEncoder) / 2; //find average error that can be applied to both sides
		integral += ((fabs(error) < controllableRange) && (error != 0)) ? (error) : (0); //if error is in a controllable range and is not equal to zero, then increment it by the current error
		derivative = error - prevError; //derivative is equal to the error minus the previous error
		prevError = error; //set previous error to current error so it can be used on the next iteration
		leftChassisSpeed -= ((error / nnKP) * useNNKP) + ((integral / nnKP) * useNNKI) + ((derivative / nnKD) * useNNKD); //decrement left speed by error times constant plus integral times constant plus derivative times constant
		rightChassisSpeed += ((error / nnKP) * useNNKP) + ((integral / nnKP) * useNNKI) + ((derivative / nnKD) * useNNKD); //increment right speed by error times constant plus integral times constant plus derivative times constant
		driveLeftChassis(leftChassisSpeed * direction * -1); //apply newly calculated speed to left chassis
		driveRightChassis(rightChassisSpeed * direction); //apply newly calculated speed to right chassis
		wait1Msec(20); //iterate 1000 divided 20 times a second (or 50 times a second)

	}

	driveLeftChassis(0);
	driveRightChassis(0);
	resetEncoders();

}
